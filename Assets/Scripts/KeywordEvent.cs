﻿using UnityEngine.Events;

[System.Serializable]
public class KeywordEvent 
{
    public string Keyword;
    public UnityEvent ActionToDo;
}
