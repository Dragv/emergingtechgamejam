﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using UnityEngine.Windows.Speech;
using System;
using System.Linq;

public class VoiceToEvents : MonoBehaviour
{
    [SerializeField]
    private KeywordEvent[] _actions;

    private Dictionary<string, UnityEvent> _keywordevents;
    private KeywordRecognizer _keywordRecognizer;

    private void Start()
    {
        PopulateDictionary();
        _keywordRecognizer = new KeywordRecognizer(_keywordevents.Keys.ToArray());
        _keywordRecognizer.OnPhraseRecognized += (args) =>
        {
            Debug.Log($"Recognized [{args.text}] with a confidence of [{args.confidence}]\nTime taken [{DateTime.Now - args.phraseStartTime}]");
            _keywordevents[args.text].Invoke();
        };
        _keywordRecognizer.Start();
    }

    private void PopulateDictionary()
    {
        _keywordevents = new Dictionary<string, UnityEvent>();

        foreach( var item in _actions)
        {
            _keywordevents[item.Keyword] = item.ActionToDo;
        }
    }
}
