using System.Collections;
using UnityEngine;
public class BlockGenerator : MonoBehaviour
{
    [SerializeField]
    private GameObject _Block;
    [SerializeField]
    private int _GridWidth;
    [SerializeField]
    private int _CubesPerLine;
    private int _LinesNumbers = 256;
    private float _Y = 0;
    private GameObject _BundleOfLines;

    private void Awake()
    {
        _BundleOfLines = new GameObject("Bundle");
        _BundleOfLines.transform.SetParent(this.transform);
        NewLine();
        //InvokeRepeating("NewLine", 1.0f, 2.0f);
    }

    public void NewLine()
    {
        Vector3 positionOffset = transform.position;
        GameObject line = new GameObject("Line");
        line.transform.SetParent(_BundleOfLines.transform);

        for (int x = 0; x < _GridWidth; x++)
        {
            GameObject block = Instantiate(_Block, positionOffset, _Block.transform.rotation, line.transform);
            block.GetComponent<Block>().SetValue(Mathf.PerlinNoise((float) x / (float) _GridWidth, (float) _Y / (float) _GridWidth));
            positionOffset.x += _Block.transform.localScale.x + 0.5f;
        }

        Move();
        _Y++;
    }
    
    private void Move()
    {
        _BundleOfLines.transform.position -= Vector3.forward * (transform.localScale.z + 0.5f);
    }
}
