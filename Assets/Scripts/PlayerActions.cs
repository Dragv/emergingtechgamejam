﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActions : MonoBehaviour
{
    public void MoveRight()
    {
        if(!GameDirector.Instance.IsPlayerTurn()) return;

        if(!Physics.Raycast(transform.position, Vector3.right, 1.5f))
        {
            transform.position += Vector3.right * 1.5f;
            GameDirector.Instance.sTurn++;
            GameDirector.Instance.tTurns++;
        }

        GetComponent<DeathCondition>().CheckDeathCondition();
    }

    public void MoveLeft()
    {
        if(!GameDirector.Instance.IsPlayerTurn()) return;

        if(!Physics.Raycast(transform.position, Vector3.left, 1.5f))
        {
            transform.position += Vector3.left * 1.5f;
            GameDirector.Instance.sTurn++;
            GameDirector.Instance.tTurns++;
        }

        GetComponent<DeathCondition>().CheckDeathCondition();
    }

    public void MoveUp()
    {
        if(!GameDirector.Instance.IsPlayerTurn()) return;

        if(!Physics.Raycast(transform.position, Vector3.forward, 1.5f))
        {
            transform.position += Vector3.forward * 1.5f;
            GameDirector.Instance.sTurn++;
            GameDirector.Instance.tTurns++;
        }

        GetComponent<DeathCondition>().CheckDeathCondition();
    }

    public void MoveDown()
    {
        if(!GameDirector.Instance.IsPlayerTurn()) return;

        if(!Physics.Raycast(transform.position, Vector3.back, 1.5f))
        {
            transform.position += Vector3.back * 1.5f;
            GameDirector.Instance.sTurn++;            
            GameDirector.Instance.tTurns++;
        }

        GetComponent<DeathCondition>().CheckDeathCondition();
    }

    public void ShootUp()
    {
        if(!GameDirector.Instance.IsPlayerTurn()) return;

        RaycastHit hit;

        if(Physics.Raycast(transform.position, Vector3.forward, out hit, Mathf.Infinity))
        {
            hit.transform.gameObject.GetComponent<Block>().Hit();
        }

        GameDirector.Instance.sTurn++;
        GameDirector.Instance.tTurns++;
    }

    public void ShootDown()
    {
        if(!GameDirector.Instance.IsPlayerTurn()) return;

        RaycastHit hit;

        if(Physics.Raycast(transform.position, Vector3.back, out hit, Mathf.Infinity))
        {
            hit.transform.gameObject.GetComponent<Block>().Hit();
        }

        GameDirector.Instance.sTurn++;
        GameDirector.Instance.tTurns++;
    }

    public void ShootLeft()
    {
        if(!GameDirector.Instance.IsPlayerTurn()) return;

        RaycastHit hit;

        if(Physics.Raycast(transform.position, Vector3.left, out hit, Mathf.Infinity))
        {
            hit.transform.gameObject.GetComponent<Block>().Hit();
        }

        GameDirector.Instance.sTurn++;
        GameDirector.Instance.tTurns++;
    }

    public void ShootRight()
    {
        if(!GameDirector.Instance.IsPlayerTurn()) return;

        RaycastHit hit;

        if(Physics.Raycast(transform.position, Vector3.right, out hit, Mathf.Infinity))
        {
            hit.transform.gameObject.GetComponent<Block>().Hit();
        }

        GameDirector.Instance.sTurn++;
        GameDirector.Instance.tTurns++;
    }
}
