﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathCondition : MonoBehaviour
{
    [SerializeField]
    private GameObject _game;

    [SerializeField]
    private GameObject _deadText;

    [SerializeField]
    private GameObject _subText;

    [SerializeField]
    private int _xLimit;

    [SerializeField]
    private int _zLimit;

    public void CheckDeathCondition()
    {
        if( Mathf.Abs(transform.position.x) > (_xLimit * 1.5f) || transform.position.z < (_zLimit * 1.5f))
        {
            _deadText.SetActive(true);
            _subText.SetActive(true);
            _game.SetActive(false);
        }
    }
}
