﻿using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class PerlinNoiseTest : MonoBehaviour
{
    [Header("Noise")]
    [SerializeField]
    private float _scale = 8.0f;
    [SerializeField]
    private bool _randomize = true;
    [SerializeField]
    private float _offsetX = 0f;
    [SerializeField]
    private float _offsetY = 0f;
    [Header("Threshold")]
    [SerializeField]
    private float _threshold = 0.5f;
    [SerializeField]
    private bool _useThreshold;
    [SerializeField]
    private int _size = 256;
    private Renderer _renderer;
    private Texture2D _texture;

    private void Awake()
    {
        _renderer = GetComponent<Renderer>();
        _texture = new Texture2D(_size, _size);
    }

    private void OnEnable()
    {
        Randomize();
    }

    [ContextMenu("Randomize")]
    private void Randomize()
    {
        if (_randomize)
        {
            _offsetX = Random.Range(0, 9999);
            _offsetY = Random.Range(0, 9999);
        }
    }

    private void Update()
    {
        GenerateTexture();
        _renderer.material.mainTexture = _texture;
    }

    private void GenerateTexture()
    {
        for (int y = 0; y < _size; y++)
        {
            for (int x = 0; x < _size; x++)
            {
                Color pixelColor = GetPerlinPixelArt(x, y);
                _texture.SetPixel(x, y, pixelColor);
            }
        }
        
        _texture.Apply();
    }

    private Color GetPerlinPixelArt(float x, float y)
    {
        var xCoord = x / (float) _size * _scale + _offsetX;
        var yCoord = y / (float) _size * _scale + _offsetY;
        float pixel = Mathf.PerlinNoise(xCoord, yCoord);

        if (_useThreshold)
        {
            if (pixel <= _threshold) pixel = 0.0f;
            else pixel = 1.0f;
        }

        return new Color(pixel, pixel, pixel);
    }
}
