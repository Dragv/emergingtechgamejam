﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPlayerCollision : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        transform.position += (transform.position - other.transform.position).normalized * 1.5f;
        GetComponent<DeathCondition>().CheckDeathCondition();
    }
}
