﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDirector : MonoBehaviour
{
    [SerializeField]
    private int _playerTurnsPerTick;

    [SerializeField]
    private int _playerTurnsWin;

    public static GameDirector Instance;

    [SerializeField]
    private BlockGenerator _blockGenerator;

    [SerializeField]
    private GameObject _game;

    [SerializeField]
    private GameObject _winText;

    [SerializeField]
    private GameObject _subText;

    internal int sTurn = 0;
    internal int tTurns = 0;

    private void Start()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    private void Update()
    {
        if(tTurns >= _playerTurnsWin)
        {
            _winText.SetActive(true);
            _subText.SetActive(true);
            _game.SetActive(false);
        }

        else
        {
            if(sTurn >= _playerTurnsPerTick)
            {
                _blockGenerator.NewLine();
                sTurn = 0;
            }
        }
    }

    public bool IsPlayerTurn()
    {
        if(sTurn < _playerTurnsPerTick) return true; else return false;
    }
}
