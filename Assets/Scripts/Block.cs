﻿using UnityEngine;

public class Block : MonoBehaviour
{
    private float value;
    private int _Hits;
    private MeshRenderer _Render;

    private void Awake()
    {
        _Render = GetComponent<MeshRenderer>();
    }

    public void SetValue(float v)
    {
        _Hits = (((int) (v * 100.0f)) / 10) - 2;
        SetColor();
    }

    private void SetColor()
    {
        if (_Hits == 1)
        {
            _Render.material.color = new Color(
                0,
                255,
                0,
                1
            );
        }
        else if(_Hits == 2)
        {
            _Render.material.color = new Color(
                0,
                0,
                255,
                1
            );
        }
        else
        {
            _Render.material.color = new Color(
                255,
                0,
                0,
                1
            );
            
        }
    }

    public void Hit()
    {
        _Hits--;
        if (_Hits == 0)
        {
            Destroy(gameObject);
        }
        SetColor();
    }
}
